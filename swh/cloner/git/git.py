# Copyright (C) 2015  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import os
import logging

from swh.cloner.git import file, proc


def clone_or_nothing(repo_url, mount_repo_folder, repo_name, witness_filename):
    """Clone a git repo_name (user/repo) repository repo_url.
    Check if a witness file exists.
    If it exists, do nothing.
    Else, check the existence of the clone.
    If it exists, it's a partial clone so it's cleaned-up.
    Otherwise, clone the repository and create a witness file after that.
    This function returns a tuple, first the full path to the local clone
    and second, the git clone command output.
    """
    full_repo_folder = os.path.join(mount_repo_folder, repo_name[0], repo_name)
    full_witness_filename = os.path.join(full_repo_folder, witness_filename)
    if os.path.isfile(full_witness_filename):
        logging.warn('Clone %s already present!' % repo_name)
        return full_repo_folder
    else:
        logging.info('Clone %s in %s' % (repo_url, full_repo_folder))
        if os.path.isdir(full_repo_folder):  # cleanup partial repository
            logging.info('Clean partial clone %s!' % repo_name)
            file.rm(full_repo_folder)

        stdout = basic_clone(repo_url, full_repo_folder)
        file.touch(full_witness_filename)
        return full_repo_folder, stdout


def basic_clone(repo_url, repo_dest_folder):
    """Clone a repository url in the destination folder `repo_dest_folder`.
    Returns the output of the git clone command.
    """
    return proc.execute(['git', 'clone', '--bare', '--quiet',
                         repo_url, repo_dest_folder])


def fsck(repo_folder):
    """Compute a git fsck on a git repository.
    Return the output of the git fsck command.
    """
    return proc.execute(['git', '--git-dir', repo_folder, 'fsck', '--full'])
