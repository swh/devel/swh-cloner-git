# coding: utf-8

# Copyright (C) 2015  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from celery import Celery
from . import conf


app = Celery('swh.cloner.git',
             include=['swh.cloner.git.worker.tasks'])


CONF = conf.read_conf()


# source: http://celery.readthedocs.org/en/latest/configuration.html
# Optional configuration, see the application user guide.
app.conf.update(
    # Default broker URL. This must be an URL in the form of:
    # transport://userid:password@hostname:port/virtual_host
    # Only the scheme part (transport://) is required, the rest is optional,
    # and defaults to the specific transports default values.
    # The transport part is the broker implementation to use, and the default
    # is amqp, which uses librabbitmq by default or falls back to pyamqp if
    # that is not installed. Also there are many other choices including redis,
    # beanstalk, sqlalchemy, django, mongodb, couchdb. It can also be a fully
    # qualified path to your own transport implementation.
    BROKER_URL=CONF['queue_url'],  # 'amqp://guest:guest@localhost:5672//'
    # Time (in seconds, or a timedelta object) for when after stored task
    # tombstones will be deleted.
    CELERY_TASK_RESULT_EXPIRES=3600,
    # Late ack means the task messages will be acknowledged after the task has
    # been executed, not just before, which is the default behavior.
    CELERY_ACKS_LATE=True,
    # The backend used to store task results (tombstones).
    # Disabled by default. Can be one of the rpc, amqp, postgres, redit, cache,
    # mongodb, cassandra, etc...
    # CELERY_RESULT_BACKEND='rpc://',
    # CELERY_RESULT_BACKEND='db+postgresql://scott:tiger@localhost/mydatabase',
    # CELERY_RESULT_PERSISTENT=True,
    # A string identifying the default serialization method to use.
    # Can be pickle (default), json, yaml, msgpack or any custom serialization
    # methods that have been registered with kombu.serialization.registry
    CELERY_ACCEPT_CONTENT=['pickle', 'json'],
    # If True the task will report its status as “started”
    # when the task is executed by a worker.
    CELERY_TRACK_STARTED=True,
    # Default compression used for task messages. Can be gzip, bzip2
    # (if available), or any custom compression schemes registered
    # in the Kombu compression registry.
    # CELERY_MESSAGE_COMPRESSION='bzip2',
    # Disable all rate limits, even if tasks has explicit rate limits set.
    # (Disabling rate limits altogether is recommended if you don’t have any
    # tasks using them.)
    CELERY_DISABLE_RATE_LIMITS=True,
    # Task hard time limit in seconds. The worker processing the task will be
    # killed and replaced with a new one when this is exceeded.
    # CELERYD_TASK_TIME_LIMIT=3600,
    # Task soft time limit in seconds.
    # The SoftTimeLimitExceeded exception will be raised when this is exceeded.
    # The task can catch this to e.g. clean up before the hard time limit
    # comes.
    CELERYD_TASK_SOFT_TIME_LIMIT=CONF['task_soft_time_limit']
)


if __name__ == '__main__':
    app.start()
