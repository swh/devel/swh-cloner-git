#!/usr/bin/env python3

# Copyright (C) 2015  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information


import argparse
import logging
import os

from swh.core import config
from swh.cloner.git.producer import clones


# Default configuration file
DEFAULT_CONF_FILE = '~/.config/swh/clones-producer.ini'


# default configuration (can be overriden by the DEFAULT_CONF_FILE)
DEFAULT_CONF = {
    'log_dir': ('string', 'swh-git-cloner/log/'),
    'db_url': ('string', 'dbname=github'),
    'repository_scheme': ('string', 'git://github.com/%s'),
    'dry_run': ('bool', False)
}


def parse_args():
    """Parse the configuration for the cli.
    """
    cli = argparse.ArgumentParser(
        description='Clone git repository on fs.')
    cli.add_argument('--verbose', '-v', action='store_true',
                     help='Verbosity level in log file.')
    cli.add_argument('--dry-run', '-n',
                     action='store_true',
                     help='Dry run (print repo only)')
    cli.add_argument('--config', '-c', help='configuration file path')

    args = cli.parse_args()

    return args


if __name__ == '__main__':
    args = parse_args()
    conf = config.read(args.config or DEFAULT_CONF_FILE, DEFAULT_CONF)
    conf['dry_run'] = args.dry_run
    config.prepare_folders(conf, 'log_dir')

    logging.basicConfig(filename=os.path.join(conf['log_dir'], 'cloner.log'),
                        level=logging.DEBUG if args.verbose else logging.INFO)

    clones.produce(conf)
