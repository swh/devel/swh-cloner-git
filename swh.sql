---
--- Software Heritage's partial data model
---

begin;

create table crawl_history (
    id       bigserial primary key,
    repo     integer,
    task_id  uuid,  -- celery task id
    date     timestamptz not null,
    duration interval,
    status   boolean,
    result   json,
    stdout   text,
    stderr   text
);

commit;
