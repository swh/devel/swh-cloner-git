#!/usr/bin/env bash

# script to run as root

apt-get install -y python3 \
        python3-pip \
        ipython3 \
        python3-psycopg2 \
        python3-nose \
        python3-celery \
        ntpdate \
        htop \
        celeryd

# beware no python3-pygit2 in stable
# (but it's now out of scope for the cloner since we use
# subprocess so out)
# apt-get install -y python3-pygit2
#         libgit2-dev \
#         libffi-dev \
#         python3-cffi
